<?php
    function convert($jfilename, $cfilename){
            //  file_get_contents  - returns the content of the json file
        if (($json = file_get_contents($jfilename)) == false)
            die("Error reading json file");
            /*json_decode - decodes the json string into array (if 2nd parameter is true - returns associative array,
            false - returns object) */
        $data = json_decode($json, true);
        /* fopen - opens file or url . 'w' - Opens the file for writing only / If the file does not exist, it tries to create it. */
        $fp = fopen($cfilename, 'w');
        $headerKeys = false;
        foreach ($data as $row){
            // empty() checks if a variable is empty
            if (empty($headerKeys)){
                //  array_keys() return an array containing keys
                $headerKeys = array_keys($row);
                // Capitalizing array's keys here 
                $capitalizeHeader =  array_map("ucfirst", $headerKeys);
                /* fputcsv - Formats as CSV and writes it to a file . 1st parameter - our opened csv file,
                2nd - array of strings(capitilized heading) */
                fputcsv($fp, $capitalizeHeader);
                // swaps keys with their values in an array
                $headerKeys = array_flip($headerKeys);
            }
            // array_merge - merge heading array and  other values
            fputcsv($fp, array_merge($headerKeys, $row));
        }
        // Closes an open file descriptor
        fclose($fp);
    }
    $date = date("d_m_Y");
    $jfilename = "task.json";
    $cfilename = $date . ".csv";

    convert($jfilename, $cfilename);

?>