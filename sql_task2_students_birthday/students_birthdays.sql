-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Jan 13, 2021 at 07:24 PM
-- Server version: 5.7.24
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `students_birthdays`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `name`) VALUES
(1, '1A'),
(2, '1B'),
(3, '1C'),
(4, '2A'),
(5, '2B'),
(6, '2C'),
(7, '3A'),
(8, '3B'),
(9, '3C'),
(10, '4A'),
(11, '4B'),
(12, '4C'),
(13, '5A'),
(14, '5B'),
(15, '5C'),
(16, '6A'),
(17, '6B'),
(18, '6C'),
(19, '7A'),
(20, '7B'),
(21, '7C'),
(22, '8A'),
(23, '8B'),
(24, '8C'),
(25, '9A'),
(26, '9B'),
(27, '9C'),
(28, '10A'),
(29, '10B'),
(30, '10C');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `classId` int(11) NOT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `firstName`, `lastName`, `classId`, `birthday`) VALUES
(1, 'Jennifer', 'Anniston', 29, '1969-02-11'),
(2, 'Courteney', 'Cox', 29, '1964-07-15'),
(3, 'Lisa', 'Kudrow', 30, '1963-07-10'),
(4, 'Matthew', 'LeBlanc', 23, '1967-07-23'),
(5, 'Matthew', 'Perry', 13, '1969-08-01'),
(6, 'David', 'Schwimmer', 14, '1966-11-02'),
(7, 'Brad', 'Pitt', 18, '1963-12-18'),
(8, 'Tom', 'Felton', 4, '1987-09-22'),
(9, 'Leonardo', 'DiCaprio', 27, '1974-11-11'),
(10, 'Daniel', 'Radcliffe', 2, '1989-07-23'),
(11, 'Johnny', 'Depp', 26, '1963-06-09'),
(12, 'Tom', 'Cruise', 21, '1962-07-23'),
(13, 'Channing', 'Tatum', 17, '1980-04-26'),
(14, 'Ryan', 'Reynolds', 13, '1976-10-23'),
(15, 'Jim', 'Carrey', 27, '1962-01-17'),
(16, 'Jackie', 'Chan', 28, '1954-04-07'),
(17, 'Stephen', 'King', 30, '1947-09-21'),
(18, 'Dan', 'Brown', 20, '1964-06-22'),
(19, 'Paulo', 'Coelho', 25, '1947-08-24'),
(20, 'Ransom', 'Riggs', 8, '2009-09-30'),
(21, 'Petro', 'Pyatochkin', 27, '2021-01-01'),
(23, 'Natalie', 'Portmar', 1, '1979-01-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `classId` (`classId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `classes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
