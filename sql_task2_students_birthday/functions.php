<?php 

    include "connection.php";
    
    function getStudentsCurrentMonth(){
        global $link;
        $query = "SELECT * FROM students WHERE MONTH(birthday) = MONTH(NOW())";
        $result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td>".$row["id"]."</td>
            <td>".$row["firstName"]."</td>
            <td>".$row["lastName"]."</td>
            <td>".$row["classId"]."</td>
            <td>".$row["birthday"]."</td>
            </tr>";
        }  
   };
   function showClassesData(){
        global $link;
        $query = "SELECT * FROM classes";
        $result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
        while($row = mysqli_fetch_assoc($result)){
            echo "<option value='" . $row['id'] . "'> " . $row['name'] . "</option>";
        }
   };
   function getPupilsHavingBirthdayCurrentMonth(){
        if(isset($_POST['id'])) {
            $id = $_POST['id'];
            global $link;
            $query = "SELECT * FROM students WHERE classId = $id AND MONTH(birthday) = MONTH(NOW())";
            $result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
            while($row = mysqli_fetch_assoc($result)) {
                echo "<tr><td>".$row["id"]."</td>
                <td>".$row["firstName"]."</td>
                <td>".$row["lastName"]."</td>
                <td>".$row["classId"]."</td>
                <td>".$row["birthday"]."</td>
                </tr>";
            }  
        }
    };

  
   function getPupilsHavingBirthdaySelectedMonth(){
    if(isset($_POST['month'])) {
        $month = $_POST['month'];
        global $link;
        $query = "SELECT * FROM students WHERE MONTH(birthday) = $month";
        $result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td>".$row["id"]."</td>
            <td>".$row["firstName"]."</td>
            <td>".$row["lastName"]."</td>
            <td>".$row["classId"]."</td>
            <td>".$row["birthday"]."</td>
            </tr>";
        }  
    };
     
    function getYoungestStudent(){
        global $link;
        $query = "SELECT * FROM students ORDER BY birthday DESC LIMIT 3";
        $result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
        while($row = mysqli_fetch_assoc($result)) {
            echo "<tr><td>".$row["id"]."</td>
            <td>".$row["firstName"]."</td>
            <td>".$row["lastName"]."</td>
            <td>".$row["classId"]."</td>
            <td>".$row["birthday"]."</td>
            </tr>";
        }  
   };

   function getOldestStudent(){
    global $link;
    $query = "SELECT * FROM students ORDER BY birthday LIMIT 3";
    $result = mysqli_query($link, $query) or die("Error " . mysqli_error($link)); 
    while($row = mysqli_fetch_assoc($result)) {
        echo "<tr><td>".$row["id"]."</td>
        <td>".$row["firstName"]."</td>
        <td>".$row["lastName"]."</td>
        <td>".$row["classId"]."</td>
        <td>".$row["birthday"]."</td>
        </tr>";
    }  
};
};


?>