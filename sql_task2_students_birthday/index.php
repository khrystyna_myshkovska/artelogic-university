<?php include "functions.php";?>


<div style="display:flex;justify-content:space-between;flex-wrap:wrap;">
    <div style="width:33%;height:300px;">
       <h4>List of students birthday in the current month by specific class</h4>
       <table style="text-align:center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fist Name</th>
                    <th>Last Name</th>
                    <th>Class ID</th>
                    <th>Birthday</th>
                </tr>
            </thead>
            <tbody>
                <?php getStudentsCurrentMonth(); ?>
            </tbody>
       </table>
    </div>

    <div style="width:33%;height:300px;">
        <h4>List of students with a birthday in the current month for a specific class</h4>
        <form action="index.php" method="post">
            <div class="form-group">
                <select name="id">
                    <option disabled selected value></option>
                    <?php showClassesData();?>
                </select>
                <input type="submit" name="submit" value = "Search">
            </div>
               
        </form>
         
        <table style="text-align:center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fist Name</th>
                    <th>Last Name</th>
                    <th>Class ID</th>
                    <th>Birthday</th>
                </tr>
            </thead>
            <tbody>
                <?php getPupilsHavingBirthdayCurrentMonth(); ?>            
            </tbody>
       </table>
    </div>




    <div style="width:33%;height:300px;">
        <h4>List of students birthday of selected month</h4>
        <form action="index.php" method="post">
            <div class="form-group">
                <select name="month">
                    <option disabled selected value></option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <input type="submit" name="submit" value = "Search">
            </div>
               
        </form>
         
        <table style="text-align:center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fist Name</th>
                    <th>Last Name</th>
                    <th>Class ID</th>
                    <th>Birthday</th>
                </tr>
            </thead>
            <tbody>
                <?php getPupilsHavingBirthdaySelectedMonth(); ?>            
            </tbody>
       </table>
    </div>
  


    <div style="width:50%;">
       <h4>List of Youngest students at school</h4>
       <table style="text-align:center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fist Name</th>
                    <th>Last Name</th>
                    <th>Class ID</th>
                    <th>Birthday</th>
                </tr>
            </thead>
            <tbody>
                <?php getYoungestStudent(); ?>
            </tbody>
       </table>
    </div>


    <div style="width:50%;">
       <h4>List of Oldest students at school</h4>
       <table style="text-align:center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Fist Name</th>
                    <th>Last Name</th>
                    <th>Class ID</th>
                    <th>Birthday</th>
                </tr>
            </thead>
            <tbody>
                <?php getOldestStudent();  ?>
            </tbody>
       </table>
    </div>
  
</div>   